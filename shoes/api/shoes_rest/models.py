from django.db import models
from django.urls import reverse

# Create your models here.

class BinVO(models.Model):
    import_href = models.CharField(
        max_length=200, 
        unique=True
    )

class Shoe(models.Model):
    manufacturer = models.CharField(max_length=200)
    model_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField(null=True)
    bin = models.ForeignKey(
        BinVO,
        on_delete=models.CASCADE
    )

    def get_api_url(self):
        return reverse("list_shoes", kwargs={"pk": self.pk})