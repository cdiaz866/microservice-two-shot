from django.urls import path

from .views import (
    list_shoes,
    detail_shoes
)

urlpatterns = [
    path("shoes/", list_shoes, name="list_shoes"),
    path("shoes/<int:pk>/", detail_shoes, name="detail_shoes"),
] 