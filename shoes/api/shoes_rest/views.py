from django.http import HttpResponse, JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Shoe, BinVO
import json
from common.json import ModelEncoder

# Create your views here.

class BinVOEncoder(ModelEncoder):
    model=BinVO
    properties=[
        "id",
        "import_href",
    ]

class ShoeDetailEncoder(ModelEncoder):
    model=Shoe
    properties=[
        "id",
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin"
    ]
    encoders={
        "bin": BinVOEncoder(),
    }

@require_http_methods(["GET", "POST"])
def list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(shoes, encoder=ShoeDetailEncoder, safe=False)
    else:
        # create a shoe
        # Shoe must be defined per the model
        # model will be filled out by User via JSON body
        # JSON will be parsed into Python Dictionary
        content = json.loads(request.body)
        # get BinVO for the bin passed by the client
        try:
            bin_href = f'/api/bins/{content["bin"]}/'
            print(bin_href)
            bin_vo = BinVO.objects.get(import_href=bin_href)
        # assign that to a key in the dictionary
            content["bin"] = bin_vo
        except BinVO.DoesNotExist:
            message = f'No matching BinVO found for the bin with an id of {content["bin"]}'
            return JsonResponse({"message": message}, status=400)
        # create new shoe with content-dictionary
        shoe = Shoe.objects.create(**content)
        # Parse into JSON and send to client
        return JsonResponse(shoe, encoder=ShoeDetailEncoder, safe=False, status=201)
    
@require_http_methods(["DELETE"])
def detail_shoes(request, pk = None):
    deleted, _deleted = Shoe.objects.filter(id=pk).delete()
    return JsonResponse({"deleted": deleted > 0})