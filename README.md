# Wardrobify

Team:

* Carlos Diaz - Hats Microservice
* Rydel Hutchins - Shoes Microservice

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

- shoes_rest.models
  * BinVO: a model that handles data that is kept in the wardrobe microservice. Data primarily consists of "bins" and "locations"
   -- import_href: the only variable that is held in this model.  It stores imported data via API from Wardrobe-Microservice
  * Shoe: a model that is used to create Shoe objects.  Shoe objects need to be placed in a bin, so this model interacts with BinVO via ForeignKey, allowing it to grab Bin and Location data from the wardrobe microservice through its API.
  {
    "manufacturer": "Nike",
    "model_name": "Air Max",
    "color": "Black",
    "picture_url": "https://media.some-url.example/this-could-be-an-image.jpeg",
    "bin": "1"
  }
    Bin should be an existing Bin, if it does not exist, you must access the API via "POST" to "http://localhost:8100/api/bins/". The model is well documented in wardrobe_api.models

  The APIs are all fairly straightforward, you interact with Shoes microservices by utilizing http://shoes-api:8000/api/shoes/ and http://shoes-api:8000/api/shoes/<id>
  Outside of Docker, the service is hosted on Port 8080, keep in mind to change that should you want to access it by other means.
  Simple GET, POST, and DELETE commands as you would expect them to behave.


## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
