import React, { useState, useEffect } from 'react';
import ShoeListItems from './ShoeListItem';

const ShoeList = function(props) {
    const [shoes, setShoes] = useState ([]);

    const getShoes = async function() {
        const url = 'http://localhost:8080/api/shoes/';
        const response = await fetch(url);
        const shoes = await response.json();
        setShoes(shoes);
    }

    const deleteShoe = async function(id) {
        const url = `http://localhost:8080/api/shoes/${id}/`;

        try {
            const res = await fetch(url, {method: 'delete'});
            if (res.ok) {
                console.log('Delete that sob' + id)
            }
        } catch(e) {
            console.log('unable to delete shoe.', e);
        }
    }

    useEffect(() => {
        getShoes();
    },[])

    return(
        <>
           <h1>Shoes</h1>
           <div className="row">
               {shoes.map(shoe => <ShoeListItems key={shoe.id} shoe={shoe} deleteShoe={deleteShoe} />)}
           </div>
        </>
    )
}

export default ShoeList;